package co.com.pragma.msapppragma;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import co.com.pragma.pojo.ClienteItem;
import co.com.pragma.pojo.DBConf;
import co.com.pragma.pojo.RequestResponse;

public class MsClienteDelete implements RequestHandler<ClienteItem, RequestResponse> {

    @Override
    public RequestResponse handleRequest(ClienteItem c, Context context) {
    	RequestResponse rr = new RequestResponse();
        try {
            DynamoDBMapper mapper = new DynamoDBMapper(DBConf.CLIENT);
            ClienteItem old = mapper.load(ClienteItem.class, c.getId());
            if (old != null) {
                mapper.delete(old);
                rr.setStatus(200);
                rr.setMessage("Registro eliminado con exito!");
            } else {
                rr.setStatus(404);
                rr.setMessage("El registro con id - " + c.getId() + " no existe!");
            }

        } catch (Exception e) {
            rr.setStatus(500);
            rr.setMessage(e.getMessage());
        }
        return rr;
    }

}
