package co.com.pragma.msapppragma;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSMessage;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.pragma.pojo.ClienteItem;
import co.com.pragma.pojo.DBConf;
import co.com.pragma.pojo.RequestResponse;

public class MsClienteUpdate implements RequestHandler<SQSEvent, RequestResponse> {

	@Override
	public RequestResponse handleRequest(SQSEvent event, Context context) {
		ObjectMapper jsMapper = new ObjectMapper();
		RequestResponse rr = new RequestResponse();
		for (SQSMessage msg : event.getRecords()) {
			try {
				ClienteItem c = jsMapper.readValue((String) msg.getBody(), ClienteItem.class);
				DynamoDBMapper mapper = new DynamoDBMapper(DBConf.CLIENT);
				ClienteItem old = mapper.load(ClienteItem.class, c.getId());
				if (old != null) {

					if (c.getTipoIdentificacion() > 0) {
						old.setTipoIdentificacion(c.getTipoIdentificacion());
					}

					if (!c.getIdentificacion().equals("")) {
						old.setIdentificacion(c.getIdentificacion());
					}

					old.setNombres(c.getNombres());
					old.setApellidos(c.getApellidos());
					old.setFechaNacimiento(c.getFechaNacimiento());
					old.setCiudadNacimiento(c.getCiudadNacimiento());
					mapper.save(old);
					rr.setStatus(200);
					rr.setMessage("Registro actualizado con exito!");
				} else {
					rr.setStatus(404);
					rr.setMessage("El registro con id - " + c.getId() + " no existe!");
				}
			} catch (Exception e) {
				rr.setStatus(500);
				rr.setMessage(e.getMessage());
			}
		}
		return rr;
	}

}
