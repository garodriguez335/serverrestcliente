package co.com.pragma.msapppragma;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import co.com.pragma.pojo.ClienteItem;
import co.com.pragma.pojo.DBConf;
import co.com.pragma.pojo.RequestResponse;

public class MsClienteGetAll implements RequestHandler<ClienteItem, RequestResponse> {
    @Override
    public RequestResponse handleRequest(ClienteItem o, Context context) {
    	RequestResponse rr = new RequestResponse();
        try {
            if (o.getId() > 0) {
            	MsClienteById handel = new MsClienteById();
                return handel.handleRequest(o, context);
            } else {
                DynamoDBMapper mapper = new DynamoDBMapper(DBConf.CLIENT);
                List<ClienteItem> itemList = new ArrayList<>();

                DynamoDBScanExpression query = new DynamoDBScanExpression();
                boolean filtro = false;
                Map<String, AttributeValue> attrs = new HashMap<>();
                StringBuilder strB = new StringBuilder();
                //**********
                if (o.getIdentificacion() != null) {
                    if (!o.getIdentificacion().equals("")) {
                        filtro = true;
                        attrs.put(":v1", new AttributeValue().withS(o.getIdentificacion()));
                        strB.append("contains(identificacion, :v1)");
                    }
                }
                if (o.getNombres() != null) {
                    if (!o.getNombres().equals("")) {
                        if (filtro) {
                            strB.append(" and ");
                        }
                        filtro = true;
                        attrs.put(":v2", new AttributeValue().withS(o.getNombres()));
                        strB.append("contains(nombres, :v2)");
                    }
                }
                if (o.getApellidos() != null) {
                    if (!o.getApellidos().equals("")) {
                        if (filtro) {
                            strB.append(" and ");
                        }
                        filtro = true;
                        attrs.put(":v3", new AttributeValue().withS(o.getApellidos()));
                        strB.append("contains(apellidos, :v3)");
                    }
                }
                if (o.getFiltro() != null) {
                    if (!o.getFiltro().equals("")) {
                        if (filtro) {
                            strB.append(" and ");
                        }
                        filtro = true;
                        String[] filtroPar = o.getFiltro().split("_");
                        int tipoOperador = Integer.parseInt(filtroPar[0]);
                        String operador = "";
                        if (tipoOperador == 0) {
                            operador = "=";
                        } else if (tipoOperador == 1) {
                            //operador = ">";
                            operador = "<";
                        } else if (tipoOperador == 2) {
                            //operador = "<";
                            operador = ">";
                        } else if (tipoOperador == 3) {
                            //operador = ">=";
                            operador = "<=";
                        } else if (tipoOperador == 4) {
                            //operador = "<=";
                            operador = ">=";
                        }
                        //***********
                        Date now = new Date();
                        int anio = Integer.parseInt(filtroPar[1]);

                        Calendar c = Calendar.getInstance();
                        c.setTime(now);
                        c.add(Calendar.YEAR, -anio);
                        Date anioEdad = c.getTime();

                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        String stdDate = df.format(anioEdad);

                        System.out.println("fecha_nacimiento "+operador+" "+anioEdad.getTime());
                        System.out.println("fecha_nacimiento "+operador+" "+stdDate);
                        attrs.put(":v4", new AttributeValue().withS(stdDate));
                        
                        strB.append("fecha_nacimiento ");
                        strB.append(operador);
                        strB.append(" :v4");
                    }
                }
                ///*******
                if (filtro) {
                    System.out.println(strB.toString());
                    query.withFilterExpression(strB.toString())
                            .withExpressionAttributeValues(attrs);
                }
                ///*******
                if (!filtro) {
                    itemList = mapper.scan(ClienteItem.class, query);
                } else {
                    itemList = mapper.parallelScan(ClienteItem.class, query, 3);
                }

                if (itemList.size() > 0) {
                    rr.setStatus(200);
                    rr.setMessage("Lista de items encontrados!");
                    rr.setData(itemList);
                } else {
                    rr.setStatus(404);
                    rr.setMessage("No hay registros!");
                }
            }
        } catch (Exception e) {
            rr.setStatus(500);
            rr.setMessage(e.getMessage());
        }
        return rr;
    }

}
