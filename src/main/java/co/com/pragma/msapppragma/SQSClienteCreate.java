package co.com.pragma.msapppragma;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.pragma.pojo.ClienteItem;
import co.com.pragma.pojo.RequestResponse;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;

public class SQSClienteCreate implements RequestHandler<ClienteItem, RequestResponse> {

	@Override
	public RequestResponse handleRequest(ClienteItem newRegistro, Context context) {
		final String myQueueUrl = "https://sqs.sa-east-1.amazonaws.com/804869790164/SQS-TEST";
		final AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
		
		RequestResponse retorno = new RequestResponse();
		
		ObjectMapper om = new ObjectMapper();
		String json = "";
		try {
			json = om.writeValueAsString(newRegistro);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			retorno.setStatus(504);
			retorno.setMessage(e.getMessage());
			return retorno;
		}

        try {
        	System.out.println("Body==>"+json);
            final SendMessageRequest sendMessageRequest = new SendMessageRequest();
            sendMessageRequest.withMessageBody(json);
            sendMessageRequest.withQueueUrl(myQueueUrl);
            sqs.sendMessage(sendMessageRequest);
            retorno.setStatus(200);
            retorno.setMessage("La peticion se esta procesando!");
        } catch (final AmazonServiceException ase) {
        	retorno.setStatus(501);
            retorno.setMessage("Se genero un problema al procesar solicitud!");
            System.out.println("Caught an AmazonServiceException, which means " +
                    "your request made it to Amazon SQS, but was " +
                    "rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (final AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means " +
                    "the client encountered a serious internal problem while " +
                    "trying to communicate with Amazon SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
            retorno.setStatus(501);
            retorno.setMessage("Se genero un problema al procesar solicitud!");
        }
        
        return retorno;
	}

}
