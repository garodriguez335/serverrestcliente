package co.com.pragma.msapppragma;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import co.com.pragma.pojo.ClienteItem;
import co.com.pragma.pojo.DBConf;
import co.com.pragma.pojo.RequestResponse;

public class MsClienteById implements RequestHandler<ClienteItem, RequestResponse> {
	
    @Override
    public RequestResponse handleRequest(ClienteItem o, Context context) {
    	RequestResponse rr = new RequestResponse();
        try {
            DynamoDBMapper mapper = new DynamoDBMapper(DBConf.CLIENT);
            ClienteItem clienteFind = mapper.load(ClienteItem.class, o.getId());
            if (clienteFind != null) {
                rr.setStatus(200);
                rr.setMessage("Item encontrado!");
                rr.setData(clienteFind);
            } else {
                rr.setStatus(404);
                rr.setMessage("No hay registros!");
            }
        } catch (Exception e) {
            rr.setStatus(500);
            rr.setMessage(e.getMessage());
        }
        return rr;
    }

}
