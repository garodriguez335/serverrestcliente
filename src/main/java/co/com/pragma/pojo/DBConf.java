package co.com.pragma.pojo;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;

public class DBConf {
	public static AmazonDynamoDB CLIENT = AmazonDynamoDBClientBuilder.standard().build();
    public static DynamoDB DYNAMODB = new DynamoDB(CLIENT);
    public static String TABLE_NAME = "clientes";
    public static Regions REGION = Regions.SA_EAST_1;
}
